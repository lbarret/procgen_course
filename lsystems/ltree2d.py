from collections import namedtuple
import base, turtle
from ltree import LRenderer
from base import RuleMgr
import math

#== Symbols

Branch      = namedtuple("Branch", 'length, base_width, top_width, color')
Trunk       = namedtuple("Trunk", 'length, base_width, top_width, color')
Left        = namedtuple("Left", 'angle')
Right       = namedtuple("Right", 'angle')
PushState   = object()
PopState    = object()

#== Rule

class Split(base.Rule):
    def match(self, cmd):
        return isinstance( cmd, Branch)

    def apply(self, cmd):
        branch = cmd
        left_angle = right_angle = 40
        return [ 
                 Trunk( branch.length, base_width=branch.base_width, top_width=branch.top_width, color=branch.color),
                 PushState,
                 Left( left_angle),
                 Branch(branch.length, base_width=branch.top_width, top_width=branch.top_width * 0.8, color=branch.color),
                 PopState,
                 PushState,
                 Right( right_angle),
                 Branch(branch.length, base_width=branch.top_width, top_width=branch.top_width * 0.8, color=branch.color),
                 PopState,
                 ]

class TurnLeft(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Left)
    
    def execute(self, cmd, context):
        turtle.left( cmd.angle)

class TurnRight(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Right)

    def execute(self, cmd, context):
        turtle.right( cmd.angle)

class DrawPushState(base.DrawCmd):
    def match(self, cmd):
        return cmd is PushState

    def execute(self, cmd, context):
        context.pos.append( (turtle.pos(), turtle.heading() ) )

class DrawPopState(base.DrawCmd):
    def match(self, cmd):
        return cmd is PopState

    def execute(self, cmd, context):
        turtle.penup()
        pos, heading = context.pos.pop()
        turtle.setpos( pos )
        turtle.setheading( heading)
        turtle.pendown()

class DrawTrunk(base.DrawCmd):
    def match(self, cmd):
        return isinstance( cmd, (Branch, Trunk))

    def execute(self, cmd, context):
        trunk = cmd
        #==
        diff      = (trunk.base_width - trunk.top_width ) / 2.0
        angle     = math.degrees(math.atan(trunk.length / diff))
        side_dist = math.sqrt( trunk.length**2 + diff**2)
        #==
        turtle.fillcolor(trunk.color)
        turtle.pencolor(trunk.color)
        turtle.begin_fill()
        turtle.left(90)
        turtle.forward(trunk.base_width/2)
        turtle.right(180-angle)
        turtle.forward(side_dist)
        turtle.right(angle)
        turtle.forward(trunk.top_width)
        turtle.right(angle)
        turtle.forward(side_dist)
        turtle.right(180-angle)
        turtle.forward(trunk.base_width/2)
        turtle.end_fill()
        turtle.right(90)
        turtle.penup()
        turtle.forward(trunk.length)
        turtle.penup()

default_renderer = LRenderer( [ DrawTrunk(),
                               TurnLeft(),
                               TurnRight(),
                               DrawPushState(),
                               DrawPopState(),
                               ] )


def test():
    start = [ Branch( 100, 70, 50, "#00ff00") ]
    rulemgr = RuleMgr( [ Split() ] )
    cmds = rulemgr.apply( start, 3)
    print cmds
    default_renderer.draw( cmds )
