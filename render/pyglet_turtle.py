import pyglet
from pyglet.gl import *

config = pyglet.gl.Config(sample_buffers=1, samples=4)
window = pyglet.window.Window(config=config, resizable=True)

#@window.event
def on_draw():
    window.clear()
    pyglet.gl.glColor4f(1.0,0,0,1.0)
    #glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    #glEnable (GL_BLEND)
    glEnable (GL_LINE_SMOOTH);
    glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE)
    glLineWidth (3)
    pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
        ('v2i', (10, 15, 300, 305))
    )
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    for i in range(0,300, 50):
        vertex_list = pyglet.graphics.vertex_list(2,
                ('v2i', (10, 15, 300+i, 350+i)),
                ('c3B', (0, 0, 255, 0, 255, 0))
        )
        vertex_list.draw(pyglet.gl.GL_LINES)

pyglet.app.run()
