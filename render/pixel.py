import numpy as np 
from PIL import Image
import itertools as it

w, h = 512,512


def create_color_array(w, h):
    data =  np.zeros( (w,h,3), dtype=np.uint8)
    return data

def color_array_to_png(data, path):
    img = Image.fromarray(data, 'RGB')
    img.save(path)

def examples(data):
    #== change the color of one coord in the color array
    data[[256,260],:] = [255,0,0]
    #== appply reds on array lines
    #reds = [ (i,0,0) for i in range(0,255, 5)]
    #idx_color = it.izip( range(data.shape[0]), it.cycle( reds ))
    #for (idx, color) in idx_color:
        #data[idx,...] = color

class ColorArray(object):
    def __init__(self, w, h):
        self.data = create_color_array(w, h)

    def save(self, path="my.png"):
        color_array_to_png( self.data, path)

    def set_pixel(self, x, y, color):
        self.data[ [x], [y], : ] = color

    def set_column(self, y, color):
        self.data[ :, [y], : ] = color

    def set_line(self, x, color):
        self.data[ x ] = color

