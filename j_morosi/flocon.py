import turtle

class Rule(object):
	def matches(self, cmd):
		# Retourne bool.
		return false
		
	def apply(self, cmd):
		# Retourne liste de commandes.
		return [cmd]
		
	def __str__(self):
		return "Rule"
		
class ExpandForward(Rule):
	def matches(self, cmd):
		code, val = cmd
		return code == 'F'
		
	def apply(self, cmd):
		code, val = cmd
		return [('F', val),('+', 60),('F', val / 2.0),('-', 60 * 2.0),('F', val / 2.0),('+', 60),('F', val)]
		
	def __str__(self):
		return "ExpandForward"
		
class RuleMgr(object):
	def __init__(self):
		self.rules = []
		self.cmds = []
		
	def apply(self, nb_iteration, cmds):
		if nb_iteration >= 1:
			for rule in self.rules:
				result = []
				for cmd in cmds:
					if rule.matches(cmd):
						result += rule.apply(cmd)
					else:
						result += [cmd]
				cmds = result
			return self.apply(nb_iteration - 1, cmds)
		else:
			return cmds
				
class DrawingCmd(object):
	def matches(self, cmd):
		# Retourne bool.
		return false
		
	def execute(self, context, cmd):
		pass
		
class DrawingForwardCmd(DrawingCmd):
	def matches(self, cmd):
		code, val = cmd
		return code == 'F'
		
	def execute(self, context, cmd):
		code, val = cmd
		turtle.forward(val)
		
class DrawingLeftCmd(DrawingCmd):
	def matches(self, cmd):
		code, val = cmd
		return code == '+'
		
	def execute(self, context, cmd):
		code, val = cmd
		turtle.left(val)
		
class DrawingRightCmd(DrawingCmd):
	def matches(self, cmd):
		code, val = cmd
		return code == '-'
		
	def execute(self, context, cmd):
		code, val = cmd
		turtle.right(val)
			
class Renderer(object):
	def __init__(self):
		self.drawingCmds = []
		
	def draw(self, cmds, context = {}):
		context['renderer'] = self
		for cmd in cmds:
			for drawingCmd in self.drawingCmds:
				if drawingCmd.matches(cmd):
					drawingCmd.execute(context, cmd)
			
def flocon(speed, n, x = 0, y = 0):
	turtle.delay(0)
	turtle.reset()
	turtle.up()
	turtle.goto(x, y)
	turtle.down()
	rm = RuleMgr()
	rm.rules = [ExpandForward()]
	cmds = rm.apply(n, [('F', speed), ('-', 120), ('F', speed), ('-', 120), ('F', speed)])
	r = Renderer()
	r.drawingCmds = [DrawingForwardCmd(), DrawingLeftCmd(), DrawingRightCmd()]
	r.draw(cmds)
