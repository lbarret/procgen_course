from lsystems import base
import random
import turtle


class Forward(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'forward', param)


class Left(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'left', param)


class Right(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'right', param)


class PushState(base.Cmd):
    def __init__(self):
        base.Cmd.__init__(self, 'push_state', None)


class PopState(base.Cmd):
    def __init__(self):
        base.Cmd.__init__(self, 'pop_state', None)


class PrimaryBranch(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'prm_branch', param)


class SecondaryBranch(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'snd_branch', param)


# Pb -> F(0.7*len)[+Sb]F(0.3*len)[+F][-F]
class Diverge(base.Rule):
    def __init__(self, angle):
        self.angle = angle

    def match(self, cmd):
        return isinstance(cmd, Forward)

    def apply(self, cmd):
        length = cmd.param / 1.8
        return [
            Forward(cmd.param),
            PushState(),
            Left(self.angle),
            Forward(length),
            PopState(),
            PushState(),
            Right(self.angle),
            Forward(length),
            PopState()
        ]


class DrawForward(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Forward)

    def execute(self, cmd, context):
        dist = cmd.param
        turtle.forward(dist)


class DrawLeft(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Left)

    def execute(self, cmd, context):
        angle = cmd.param
        turtle.left(angle)


class DrawRight(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Right)

    def execute(self, cmd, context):
        angle = cmd.param
        turtle.right(angle)


class TreeRenderer(base.Renderer):
    def __init__(self, drawing_cmds):
        base.Renderer.__init__(self, drawing_cmds)
        self.context = {'state': []}

    def get_context(self):
        return self.context

    def pre_draw(self, context):
        turtle.penup()
        turtle.goto(0, -400)
        turtle.setheading(90)
        turtle.clear()
        turtle.tracer(8, 0)
        turtle.hideturtle()
        turtle.pendown()

    def post_draw(self, context):
        turtle.update()
        turtle.done()


class PushRendererState(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, PushState)

    def execute(self, cmd, context):
        context['state'].append((turtle.pos(), turtle.heading()))


class PopRendererState(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, PopState)

    def execute(self, cmd, context):
        ctx = context['state'].pop()
        turtle.penup()
        turtle.setpos(ctx[0])
        turtle.setheading(ctx[1])
        turtle.pendown()


def test(length=175, angle=60, nb_iter=7):
    turtle.reset()

    start_cmd = [Forward(length), Forward(length), Forward(length)]
    rule_manager = base.RuleMgr([Diverge(angle)])
    renderer = TreeRenderer([
        DrawForward(),
        DrawLeft(),
        DrawRight(),
        PushRendererState(),
        PopRendererState(),
    ])
    result = rule_manager.apply(start_cmd, nb_iter)
    print result
    renderer.draw(result)

if __name__ == "__main__":
    test()
