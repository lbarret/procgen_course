import pyglet
from pyglet import gl 

config = pyglet.gl.Config(sample_buffers=1, samples=4)
window = pyglet.window.Window(config=config, resizable=True)

from math import cos, sin, radians



def iso_tri(turtle, length):
    turtle.forward(length)
    turtle.rotate(120)
    turtle.forward(length)
    turtle.rotate(120)
    turtle.forward(length)

class Turtle(object):
    def __init__(self, start_pos):
        self.reset(start=start_pos)

    def reset(self, start= (0,0)):
        self.current_vector = (1,0)
        self.current_pos = start
        self.points = [self.current_pos]

    def rotate(self, angle):
        #==
        x = self.current_vector[0]
        y = self.current_vector[1]
        #==
        theta = radians(angle);
        cs = cos(theta);
        sn = sin(theta);
        #==
        px = x * cs - y * sn; 
        py = x * sn + y * cs;
        self.current_vector = (px, py)

    def forward(self, dist):
        vx = self.current_vector[0]
        vy = self.current_vector[1]
        #==
        x = self.current_pos[0]
        y = self.current_pos[1]
        #==
        self.current_pos = (x + vx*dist, y + vy*dist )
        self.points.append( self.current_pos )
        tick()

turtle = Turtle((100,100))

def set_point(pts):
    turtle.points = pts
    tick()


@window.event
def on_draw():
    window.clear()
    gl.glColor4f(1.0,0,0,1.0)
    #glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    #glEnable (GL_BLEND)
    gl.glEnable (gl.GL_LINE_SMOOTH);
    gl.glHint (gl.GL_LINE_SMOOTH_HINT, gl.GL_DONT_CARE)
    gl.glLineWidth (3)
    pts = []
    for (x,y) in turtle.points:
         pts.append(x)
         pts.append(y)
    pyglet.graphics.draw(len(turtle.points), pyglet.gl.GL_LINE_STRIP, ('v2f', tuple(pts)))

    #pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
        #('v2i', (10, 15, 300, 305))
    #)
    #gl.glHint(gl.GL_LINE_SMOOTH_HINT, gl.GL_NICEST);
    #for i in range(0,300, 50):
        #vertex_list = pyglet.graphics.vertex_list(2,
                #('v2i', (10, 15, 300+i, 350+i)),
                #('c3B', (0, 0, 255, 0, 255, 0))
        #)
        #vertex_list.draw(pyglet.gl.GL_LINES)

def tick():
    pyglet.clock.tick()
    for window in pyglet.app.windows:
        window.switch_to()
        window.dispatch_events()
        window.dispatch_event('on_draw')
        window.flip()

def run():
    pyglet.app.run()

if __name__ == "__main__":
    run()
